#!/usr/bin/env python

"""
Use this profile to instantiate an experiment using the OpenAirInterface 5G NR code base:

https://gitlab.eurecom.fr/oai/openairinterface5g/wikis/5g-nr-development-and-releases

This profile includes the following resources in a lab setting:

  * SDR UE (d430 compute + X300 USRP) running OAI ('rue1')
  * SDR eNodeB (d430 compute + x300 USRP) running OAI ('enb1')

Note: This profile is still functional but uses a fairly dated version of the OAI code base.

Instructions:

###Environment setup

Be sure to setup your SSH keys as outlined in the manual; it's better
to log in via a real SSH client to the nodes in your experiment.
Also, to get the "softscope" to work for the UE, you will
need to have X11 working.

###Running OAI

The Open Air Interface source is located under /opt/oai on the enb1
and rue1 nodes.  It is mounted as a clone of a remote blockstore
(dataset) maintained by Powder.  Feel free to change anything in
here, but be aware that your changes will not persist when your
experiment terminates.

Once your experiment is in the "Ready" state, open ssh sessions to
both the enb1 and rue1 nodes. From there, you will be able to start
OAI enb and ue services as follows:

on 'enb1'

    cd /opt/oai/openairinterface5g

    bash

    source oaienv

    cd cmake_targets/

    sudo ./build_oai -I -c -C --eNB -w USRP --noS1 -x

    sudo sysctl -w net.core.wmem_max=24862979
    
    sudo /local/repository/bin/enb.start.sh 
    
on 'rue1'

    cd /opt/oai/openairinterface5g

    bash

    source oaienv

    cd cmake_targets/

    sudo ./build_oai -I -c -C --UE -w USRP --noS1 -x
    
    sudo sysctl -w net.core.wmem_max=24862979
    
    sudo /local/repository/bin/ue.start.sh 

To disable ue software scope, just remove "-d" of following line in file /local/repository/bin/ue.start.sh:

screen -c $OAIETCDIR/enb.screenrc -L -S ue -h 10000 /bin/bash -c "sudo $ENBEXEPATH -U  -C 2680000000 --ue-txgain 85 --ue-rxgain 90 --ue-scan-carrier -r50 -E --phy-test -d"


"""

#
# Standard geni-lib/portal libraries
#
import geni.portal as portal
import geni.rspec.pg as rspec
import geni.rspec.emulab as elab
import geni.urn as URN
import geni.rspec.emulab.pnext as PN

#
# Globals
#
class GLOBALS(object):
    OAI_NR_ENB_DS = "urn:publicid:IDN+emulab.net:powdersandbox+ltdataset+oai-nr-enb"
    OAI_NR_UE_DS = "urn:publicid:IDN+emulab.net:powdersandbox+ltdataset+oai-nr-ue"
    OAI_NR_IMG = "urn:publicid:IDN+emulab.net+image+PowderSandbox//OAI-NR"
    NUC_HWTYPE = "d430"

def connectOAI_DS(node, type):
    # Create remote read-write clone dataset object bound to OAI dataset
    bs = request.RemoteBlockstore("ds-%s" % node.name, "/opt/oai")
    if type == 1:
	    bs.dataset = GLOBALS.OAI_NR_ENB_DS
    else:
	    bs.dataset = GLOBALS.OAI_NR_UE_DS
    
    bs.rwclone = True

    # Create link from node to OAI dataset rw clone
    node_if = node.addInterface("dsif_%s" % node.name)
    bslink = request.Link("dslink_%s" % node.name)
    bslink.addInterface(node_if)
    bslink.addInterface(bs.interface)
    bslink.vlan_tagging = True
    bslink.best_effort = True

#
# This geni-lib script is designed to run in the PhantomNet Portal.
#
pc = portal.Context()

#
# Profile parameters.
#
pc.defineParameter("ENODEB_COMP", "Compute node for eNodeB",
                   portal.ParameterType.STRING, "",  advanced=True,
                   longDescription="Input the name of a specific compute node to allocate for the eNodeB.  Leave blank to use default or to let the mapping algorithm choose.")
pc.defineParameter("ENODEB_SDR", "SDR for eNodeB",
                   portal.ParameterType.STRING, "",  advanced=True,
                   longDescription="Input the name of a specific SDR to allocate for the eNodeB.  Leave blank to use the default or to let the mapping algorithm choose.")           
pc.defineParameter("UE_COMP", "Compute node for UE",
                   portal.ParameterType.STRING, "",  advanced=True,
                   longDescription="Input the name of a specific compute node to allocate for the UE.  Leave blank to use default or to let the mapping algorithm choose.")
pc.defineParameter("UE_SDR", "SDR for UE",
                   portal.ParameterType.STRING, "",  advanced=True,
                   longDescription="Input the name of a specific SDR to allocate for the UE.  Leave blank to use the default or to let the mapping algorithm choose.")                   

params = pc.bindParameters()

#
# Give the library a chance to return nice JSON-formatted exception(s) and/or
# warnings; this might sys.exit().
#
pc.verifyParameters()

#
# Create our in-memory model of the RSpec -- the resources we're going
# to request in our experiment, and their configuration.
#
request = pc.makeRequestRSpec()

#
# Add compute for eNodeB
#
enb1 = request.RawPC("enb1")
if params.ENODEB_COMP:
    enb1.component_id = params.ENODEB_COMP
enb1.hardware_type = GLOBALS.NUC_HWTYPE
enb1.disk_image = GLOBALS.OAI_NR_IMG
connectOAI_DS(enb1,1)
enb1_usrp_if = enb1.addInterface( "usrp_if" )
enb1_usrp_if.addAddress( rspec.IPv4Address( "192.168.30.1", "255.255.255.0" ) )

#
# Add X300 USRP for eNodeB
#
usrp_enb = request.RawPC( "usrp_enb")
if params.ENODEB_SDR:
	usrp_enb.component_id = params.ENODEB_SDR
else:
	usrp_enb.component_id = 'pnbase2'
usrp_enb.hardware_type = "sdr"
usrp_enb.disk_image = URN.Image(PN.PNDEFS.PNET_AM, "emulab-ops:GENERICDEV-NOVLANS")
usrp_enb_if = usrp_enb.addInterface( "usrp-nuc" )
usrp_enb_if.addAddress( rspec.IPv4Address( "192.168.30.2", "255.255.255.0" ) )

#
# Add link between compute and SDR
#
usrplink_enb = request.Link( "usrp-sdr_enb" )
usrplink_enb.addInterface( enb1_usrp_if )
usrplink_enb.addInterface( usrp_enb_if )

#
# Add compte for UE
#
rue1 = request.RawPC("rue1")
if params.UE_COMP:
    rue1.component_id = params.UE_COMP
rue1.hardware_type = GLOBALS.NUC_HWTYPE
rue1.disk_image = GLOBALS.OAI_NR_IMG
connectOAI_DS(rue1,0)
rue1_usrp_if = rue1.addInterface( "usrp_if" )
rue1_usrp_if.addAddress( rspec.IPv4Address( "192.168.30.1", "255.255.255.0" ) )

#
# Add X300 USRP for UE
#
usrp_ue = request.RawPC( "usrp_ue" )
if params.UE_SDR:
	usrp_ue.component_id = params.UE_SDR
else:
	usrp_ue.component_id = 'pnbase1'
usrp_ue.hardware_type = "sdr"
usrp_ue.disk_image = URN.Image(PN.PNDEFS.PNET_AM, "emulab-ops:GENERICDEV-NOVLANS")
usrp_ue_if = usrp_ue.addInterface( "usrp-nuc" )
usrp_ue_if.addAddress( rspec.IPv4Address( "192.168.30.2", "255.255.255.0" ) )

#
# Add link between compute and SDR
#
usrplink_ue = request.Link( "usrp-sdr_ue" )
usrplink_ue.addInterface( rue1_usrp_if )
usrplink_ue.addInterface( usrp_ue_if )

#
# Print and go!
#
pc.printRequestRSpec(request)
